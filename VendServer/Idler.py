#!/usr/bin/env python

import string, time, os
from subprocess import Popen, PIPE
from random import random
from .MessageKeeper import MessageKeeper

orderings = None

IDLER_TEXT_SPEED=1.8

class Idler:
    def __init__(self, v, affinity=None):
        self.v = v
        if affinity:
            self._affinity = affinity
        else:
            self._affinity = 1

    def __next__(self):
        """Displays next stage of the idler. Returns time to the next step"""
        return 1

    def reset(self):
        """Resets the idler to a known intial state"""
        pass
    
    def finished(self):
        """Returns True if the idler is considered finished"""
        return False

    def affinity(self):
        """How much we want this idler to be the next one chosen"""
        return self._affinity

class GreetingIdler(Idler):
    def __init__(self, v, secs_to_greeting = None):
        affinity = 0 
        Idler.__init__(self, v, affinity = affinity)
        self.secs_to_greeting = secs_to_greeting
        self.message_displayed = False

    def __next__(self):
        if not self.secs_to_greeting is None:
            x = self.secs_to_greeting
            self.secs_to_greeting = None
            return x

        self.v.display('UCC SNACKS')
        self.message_displayed = True
        return 5

    def reset(self):
        self.message_displayed = False
        self.secs_to_greeting = None

    def finished(self):
        return self.message_displayed

class TrainIdler(Idler):
    def __init__(self, v):
        Idler.__init__(self, v)
        self.idle_state = 0

    def put_shark(self, s: str, l: int):
        if self.s[l] == ' ':
            self.s[l] = s
        elif self.s[l] == 'X':
            self.s[l] = '*'
        else:
            self.s[l] = 'X'

    def __next__(self):
        # does the next stage of a dance
        self.s = [' ']*10
        shark1 = self.idle_state % 18
        if shark1 < 9:
            self.put_shark('^', shark1)
        else:
            self.put_shark('^', 18-shark1)

        shark2 = ((self.idle_state+4) % 36)//2
        if shark2 < 9:
            self.put_shark('<', shark2)
        else:
            self.put_shark('<', 18-shark2)

        shark3 = ((self.idle_state+7) % 54)//3
        if shark3 < 9:
            self.put_shark('>', 9-shark3)
        else:
            self.put_shark('>', 9-(18-shark3))

        train1 = ((self.idle_state%(18*36)))
        train1_start = 122
        if train1 > train1_start and train1 < train1_start+(10*2):
            for i in range(5):
                ptr = i+train1-train1_start-5
                if ptr >= 0 and ptr < 10: self.s[ptr] = '#'

        train2 = ((self.idle_state%(18*36)))
        train2_start = 400
        if train2 > train2_start and train2 < train2_start+(10*2):
            for i in range(5):
                ptr = i+train2-train2_start-5
                if ptr >= 0 and ptr < 10: self.s[9-ptr] = '#'

        train3 = ((self.idle_state%(18*36)))
        train3_start = 230
        if train3 > train3_start and train3 < train3_start+(10*2):
            for i in range(10):
                ptr = i+train3-train3_start-10
                if ptr >= 0 and ptr < 10: self.s[ptr] = '-'

        self.v.display(''.join(self.s))
        self.idle_state += 1
        self.idle_state %= 18*36*54

    def reset(self):
        self.idle_state = 0

class OrderMaker:
    def __init__(self, n=8):
        self.n = n
        self.make_factorials(n)
    
    def make_factorials(self, n):
        self.factorial = []
        a = 1
        for i in range(1,n+1):
            self.factorial.append(a)
            a *= i

    def order(self, index):
        used = []
        for i in range(0,self.n):
            used.append(i)
        i = self.n-1
        j = 0
        res = []
        while i >= 0:
            a = index // self.factorial[i]
            index %= self.factorial[i]
            res.append(a+1)
            i -= 1
            j += 1
        for i in range(0,self.n):
            tmp = used[res[i]-1]
            for j in range(res[i],self.n):
                used[j-1] = used[j]
            res[i] = tmp
        return res

    def __getitem__(self, i):
        return self.order(i)

class GrayIdler(Idler):
    def __init__(self, v, one=None, zero=None, reorder=0):
        Idler.__init__(self, v)
        self.bits = 8
        self.size = 1 << self.bits
        self.i = 0
        self.grayCode = 0
        self.one = one
        self.zero = zero
        self.reorder = reorder
        global orderings
        if not orderings:
            orderings = OrderMaker()

    def __next__(self):
        output = self.do_next_state()
        # does the next stage of a dance
        if self.zero:
            output = output.replace("0", self.zero)
        if self.one:
            output = output.replace("1", self.one)
        if self.reorder:
            global orderings
            newoutput = ""
            for i in range(0,8):
                newoutput += output[orderings[self.reorder][i]]
            output = newoutput
        self.v.display(" %8.8s " % (output,))
        self.i = (self.i + 1) % self.size

    def do_next_state(self):
        self.grayCode = self.i ^ (self.i >> 1)
        output = self.dec2bin(self.grayCode)

        return "0"*(self.bits-len(output))+output


    def dec2bin(self,num):
        """Convert long/integer number to binary string.

        E.g. dec2bin(12) ==> '1100'.
        
        from http://starship.python.net/~gherman/playground/decbingray/decbingray.py"""

        assert num >= 0, "Decimal number must be >= 0!"

        # Gracefully handle degenerate case.
        # (Not really needed, but anyway.)    
        if num == 0:
            return '0'

        # Find highest value bit.
        val, j = 1, 1
        while val < num:
            val, j = val*2, j+1

        # Convert.
        bin = '' 
        i = j - 1
        while i + 1:
            k = pow(2, i)
            if num >= k:
                bin = bin + '1'
                num = num - k
            else:
                if len(bin) > 0:
                    bin = bin + '0'
            i = i - 1

        return bin

    def reset(self):
        self.i = 0
        self.grayCode = 0
        if self.reorder:
            self.reorder = int(random()*40319)+1


class StringIdler(Idler):
    def __init__(self, v, text="Hello Cruel World!  ",repeat=True, affinity=None):
        Idler.__init__(self, v, affinity=affinity)
        self.mk = MessageKeeper(v)
        self.text = "         " + self.clean_text(text) + "          "
        
        msg = [("",False, None),(self.text, repeat, IDLER_TEXT_SPEED)]
        self.mk.set_messages(msg)

    def clean_text(self, text: str):
        # nothing like a bit of good clean text :)
        valid = string.digits \
            + string.ascii_letters \
            + string.punctuation \
            + " "
        # uppercase it
        text = text.upper()
        clean = ""
        for char in text:
            if char in valid:
                clean = clean + char
            else:
                clean = clean + " "
        return clean

    def __next__(self):
        self.mk.update_display()

    def finished(self): 
        return self.mk.done()

class ClockIdler(Idler):
    def __init__(self, v):
        affinity = 3 
        Idler.__init__(self, v, affinity = affinity)
        self.last = None

    def __next__(self):
        colonchar = ':'
        if int(time.time()*2) & 1: colonchar = ' '
        output = time.strftime("%%H%c%%M%c%%S"%(colonchar,colonchar))
        if output != self.last:
            self.v.display(" %8.8s " % (output))
            self.last = output

class FortuneIdler(StringIdler):
    def __init__(self, v, affinity = 30):
        fortune = "/usr/games/fortune"
        text = "I broke my wookie...."
        if os.access(fortune,os.F_OK|os.X_OK):
            (lines, unused) = Popen((fortune,), close_fds=True, stdout=PIPE).communicate()
            text = lines.decode('utf-8').replace('\n', '  ').replace('\r', '')
        StringIdler.__init__(self, v, text,repeat=False, affinity=affinity)

    def reset(self):
        self.__init__(self.v, affinity=self._affinity)


class PipeIdler(StringIdler):
    def __init__(self, v, command, args, affinity = 5):
        text = "I ate my cookie...."
        if os.access(command,os.F_OK|os.X_OK):
            (lines, unused) = Popen([command,] + args.split(), close_fds=True, stdout=PIPE).communicate()
            text = lines.decode('utf-8').replace('\n', '  ').replace('\r', '')
        StringIdler.__init__(self, v, text,repeat=False, affinity=affinity)

class FileIdler(StringIdler):
    def __init__(self, v, thefile=None, repeat=False, affinity=8):
        text = "I broke my wookie...."

        if os.access(thefile,os.F_OK|os.R_OK):
            f = open(thefile,'r')
            text = "".join(f.readlines())
            f.close()
        StringIdler.__init__(self, v, text,repeat=False, affinity=affinity)
