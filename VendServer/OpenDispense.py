"""
Author: Mitchell Pomery (bobgeorge33)

System to connect to OpenDispense2.
Most of this code has been copied out of VendServer.py, then had variables updated so that it runs.
This is so VendServer can easily operate regardless of the current accounting backend.
Documentation for this code can be found inder Dispence.DispenceInterface
"""

from .DispenseInterface import DispenseInterface
import os
import logging
import re
import pwd
import base64
import socket

DISPENSE_ENDPOINT = ("localhost", 11020)

# A list of cards that should never be registered, and should never log in
# - Some of these might have been registered before we knew they were duplicates
CARD_BLACKLIST = [
	'AAAAAA==',	# All zeroes, don't allow that.
	'ISIjJA==', # CommBank credit cards
	]

class OpenDispense(DispenseInterface):
	_username = ""
	_disabled = True
	_loggedIn = False
	_userId = None

	def __init__(self, username=None, secret=False):
		pass

	def authUserIdPin(self, userId: str, pin: str):
		return self.authUserIdPin_db(userId, pin)
		#return self.authUserIdPin_file(userId, pin)
	
	def _connect(self, authenticate:bool=True, set_euid:bool=False):

		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
		try:
			sock.connect(DISPENSE_ENDPOINT)
		except ConnectionRefusedError:
			logging.error("Cannot connect to dispsrv on {}".format(DISPENSE_ENDPOINT,))
			return None
		logging.debug('connected to dispsrv')
		conn = Connection( sock.makefile('rw', encoding='utf-8') )
		if authenticate:
			rsp = conn.send_command("AUTHIDENT")
			if not "200" in rsp:
				logging.info('Server said no to AUTHIDENT! - %r' % (rsp,))
				return None
			logging.debug('authenticated')
		if set_euid:
			rsp = conn.send_command("SETEUSER %s" % (self._username,))
			if not "200" in rsp:
				logging.info('Server said no to SETEUSER! - %r' % (rsp,))
				return None
		
		return conn

	def authUserIdPin_db(self, userId: str, pin: str):
		userId = int(userId)

		try:
			# Get username (TODO: Store the user ID in the dispense database too, so the vending machine
			# doesn't need LDAP/AD working)
			info = pwd.getpwuid(userId)
		except KeyError:
			logging.info('getting pin for uid %d: user not in password file'%userId)
			return False
		
		conn = self._connect()
		if conn is None:
			logging.error("getting pin for uid {}: Unable to open connection".format(userId))
			return False
		rsp = conn.send_command("PIN_CHECK %s %s" % (info.pw_name, pin,))
		if not "200" in rsp:
			logging.info('checking pin for uid %d: Server said no (PIN_CHECK) - %r' % (userId, rsp))
			return False
		#Login Successful
		logging.info('accepted pin for uid %d \'%s\'' % (userId, info.pw_name))
		self._userid = userId
		self._loggedIn = True
		self._disabled = False
		self._username = info.pw_name
		return True

	def authUserIdPin_file(self, userId: str, pin: str):
		userId = int(userId)

		try:
			# Get info from 
			info = pwd.getpwuid(userId)
		except KeyError:
			logging.info('getting pin for uid %d: user not in password file'%userId)
			return False

		if info.pw_dir == None: return False
		pinfile = os.path.join(info.pw_dir, '.pin')
		try:
			s = os.stat(pinfile)
		except OSError:
			logging.info('getting pin for uid %d: .pin not found in home directory'%userId)
			return False
		if s.st_mode & 0o77:
			logging.info('getting pin for uid %d: .pin has wrong permissions. Fixing.'%userId)
			os.chmod(pinfile, 0o600)
		try:
			f = open(pinfile)
		except IOError:
			logging.info('getting pin for uid %d: I cannot read pin file'%userId)
			return False
		pinstr = f.readline().strip()
		f.close()
		if not re.search('^[0-9]{4}$', pinstr):
			logging.info('getting pin for uid %d: %s not a good pin'%(userId,repr(pinstr)))
			return False

		if pinstr == str(pin):
			#Login Successful
			self._userid = userId
			self._loggedIn = True
			self._disabled = False
			self._username = info.pw_name
			return True
		
		# Login Unsuccessful
		return False

	def authMifareCard(self, cardId):
		self._loggedIn = False
		self._username = None
	
		card_base64 = base64.b64encode(cardId).decode('utf-8')

		if card_base64 in CARD_BLACKLIST:
			logging.info("Blacklisted card base64:%s" % (card_base64,))
			return False
		
		conn = self._connect()
		if conn is None:
			logging.error("getting username for card {}: Unable to open connection".format(card_base64))
			return False
		cmd = "AUTHCARD %s" % (card_base64,)
		rsp = conn.send_command(cmd)
		if not rsp.startswith("200 "):
			logging.info("%s failed: Rejected card base64:%s: rsp %r" % (cmd, card_base64, rsp))
			return False
		username = rsp.split('=')[1].strip()
		logging.info("Accepted card base64:%s for %s" % (card_base64,username,))

		## Get UID for the username (not needed?)
		#try:
		#	# Get info from the system (by username)
		#	info = pwd.getpwnam(username)
		#except KeyError:
		#	logging.info('getting info for user \'%s\': user not in password file' % (username,))
		#	return False
		#self._userid = info.pw_uid
		self._userid = None
		self._username = username

		# If we get this far all is good
		self._loggedIn = True
		self._disabled = False
		return True

	def logOut(self):
		self._loggedIn = False
		self._disabled = False
		self._userId = None
		self._username = None

	def addCard(self, cardId):
		if not self.isLoggedIn():
			return False
		
		card_base64 = base64.b64encode(cardId).decode('utf-8')
		if card_base64 in CARD_BLACKLIST:
			logging.info("Blacklisted card base64:%s" % (card_base64,))
			return False
		logging.info('Enrolling card base64:%s to uid %s (%s)' % (card_base64, self._userId, self._username))
		conn = self._connect(set_euid=True)
		if conn is None:
			logging.warn("Enrolling card failed: Unable to connect")
			return False
		rsp = conn.send_command("CARD_ADD %s" % (card_base64,))
		if "200" in rsp:
			return True
		else:
			logging.warn("Enrolling card failed: Response = {}".format(rsp))
			return False

	def isLoggedIn(self):
		return self._loggedIn

	def getUsername(self):
		return self._username

	def getBalance(self):
		# Balance checking
		if not self.isLoggedIn():
			return "?"
		
		conn = self._connect(authenticate=False)
		if conn is None:
			return "?"
		cmd = "USER_INFO {}".format(self._username)
		rsp = conn.send_command(cmd)
		try:
			code,rest = rsp.split(" ", 1)
			if code != "202":
				raise ValueError("Code not 202")
			_user,_name,balance,flags = rest.split(" ")
			return "{:.2f}".format(int(balance) / 100)
		except ValueError as e:
			logging.warn("OpenDispense: {!r} response malformed ({!r}) - exception {}".format(cmd, rsp, e))
			return "?"

	def getItemInfo(self, itemId: str):
		logging.debug("getItemInfo(%s)" % (itemId,))
		itemId = OpenDispenseMapping.vendingMachineToOpenDispense(itemId)

		conn = self._connect(authenticate=False)
		if conn is None:
			return ("dead", 0,)
		cmd = "ITEM_INFO {}".format(itemId)
		rsp = conn.send_command(cmd)
		try:
			code,rest = rsp.split(" ", 1)
			if code != "202":
				raise ValueError("Code not 202")
			_item,itemid,status,price_cents,name = rest.split(" ", 4)
			return (name, int(price_cents),)
		except ValueError as e:
			logging.warn("OpenDispense: {!r} response malformed ({!r}) - exception {}".format(cmd, rsp, e))
			return ("dead", 0,)

	def isDisabled(self):
		return self._disabled

	def dispenseItem(self, itemId):
		logging.debug("getItemInfo(%s)" % (itemId,))
		if not self.isLoggedIn() or self.getItemInfo(itemId)[0] == "dead":
			return "999"

		conn = self._connect(set_euid=True)
		if conn is None:
			return "999"

		cmd = "DISPENSE {}".format( OpenDispenseMapping.vendingMachineToOpenDispense(itemId) )
		rsp = conn.send_command(cmd)
		try:
			code,rest = rsp.split(" ", 1)
			return code
		except ValueError as e:
			logging.warn("OpenDispense: {!r} response malformed ({!r}) - exception {}".format(cmd, rsp, e))
			return 999
	def openDoor(self):
		if not self.isLoggedIn():
			return False

		conn = self._connect(set_euid=True)
		if conn is None:
			return False

		cmd = "DISPENSE door:0".format()
		rsp = conn.send_command(cmd)
		try:
			code,rest = rsp.split(" ", 1)
			if code == "200":
				return True
			if code == "402":	# "Poor You"
				return False
			raise ValueError("Unknown code")
		except ValueError as e:
			logging.warn("OpenDispense: {!r} response malformed ({!r}) - exception {}".format(cmd, rsp, e))
			return False

class Connection(object):
	def __init__(self, sockf):
		self.sockf = sockf
	def send_command(self, command):
		self.sockf.write(command)
		self.sockf.write("\n")
		self.sockf.flush()
		return self.sockf.readline()

"""
This class abstracts the idea of item numbers.
It removes the need for VendServer to know the mappings between inputted numbers
and the equivalent itemId in OpenDispense.
"""
class OpenDispenseMapping():

	@staticmethod
	def vendingMachineToOpenDispense(itemId):
		logging.debug("vendingMachineToOpenDispense(%s)" % (itemId,))
		_mappingFile = "OpenDispenseMappings.conf"
		try:
			fh = open(_mappingFile)
		except IOError:
			if itemId[1] == '8':
				return 'coke:' + itemId[0]
			elif itemId[1] == '5':
				if itemId[0] == '5':
					return 'door'
				else:
					return None
			else:
				return 'snack:' + itemId
		map = ""
		for line in fh:
			#line = line.strip()
			if line.startswith(str(itemId) + " "):
				map = line[len(str(itemId)) + 1:].strip()
				print(map)
		return map


# vim: noexpandtab ts=4 sw=4
