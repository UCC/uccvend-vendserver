#!/usr/bin/env python2.5

'''mifare - a library for interacting with MIFARE readers.
Written by David Adam <zanchey@ucc.gu.uwa.edu.au>
Requires Python 2.5.

Licensed under an MIT-style license: see LICENSE file for details.
'''

import serial, logging
from functools import reduce

xor = lambda x, y: x ^ y
def checksum(data: bytes):
    return bytes([reduce(xor, data)])


class MIFAREException(Exception):
    pass


class MIFARECommunicationException(MIFAREException):
    pass


class MIFAREAuthenticationException(MIFAREException):
    pass


class MIFAREReader:
    '''An interface to a particular MIFARE reader.'''

    def __init__(self, io):
        '''Returns an interface to a MIFARE reader given a file-like object.
        The file-like object is generally a pyserial Serial object.'''
        self.io = io
        if isinstance(self.io, serial.Serial):
            self.io.setTimeout = 2
        self.address = b'\x00\x00'

    def get_absolute_block(self, vector):
        if vector[0] < 32:
            return vector[0] * 4 + vector[1]
        else:
            # Sectors below 32 are 4 blocks
            # Sectors above are 16 blocks
            # Thus, sector 32 starts at block 128, 33 at 144, and so on
            return 128 + (vector[0] - 32) * 16 + vector[1]

    def send_packet(self, data: bytes):
        '''Constructs a packet for the supplied data string, sends it to the
        MIFARE reader, then returns the response (if any) to the commmand.'''

        # Occasionally the reader inserts extra trailing characters into its
        # responses, so flush the buffers if possible beforehand.
        if isinstance(self.io, serial.Serial):
            self.io.flushInput()
            self.io.flushOutput()

        # XXX - Needs more error checking.
        data = b'\x00' + self.address + data
        packet = bytes([0xAA, 0xBB, len(data)]) + data + checksum(data)
        #print("send_packet: {!r}".format(packet))
        self.io.write(packet)
        response = ''
        header = self.io.read(2)
        if header == b'\xaa\xbb':
            length = self.io.read(1)[0]
            data = self.io.read(length)
            packet_xsum = self.io.read(1)
            if checksum(data) == packet_xsum and len(data) == length:
                # Strip off separator and address header
                return data[3:]
            else:
                raise MIFARECommunicationException("Invalid response received")

    def set_antenna(self, state = True):
        """Turn the card reader's antenna on or off (no return value)"""
        command = bytes([0xc, 0x1, int(state)])
        response = self.send_packet(command)
        if response == b'\x0c\x01\x00':
            return None
        else:
            raise MIFAREException('command failed: set_antenna (%s)' % state)

    def select_card(self, include_halted = False):
        """Selects a card and returns a tuple of  (serial number, capacity).

        If include_halted is set, may select a card that halt() has previously
        been called on."""

        # Request type of card available
        command = command = b'\x01\x02'
        if include_halted:
            command += b'\x52'
        else:
            command += b'\x26'

        card_type_response = self.send_packet(command)

        if card_type_response == None or card_type_response[2] == 0x14:
            raise MIFAREException("select_card: no card available")
        card_type = card_type_response[3:5]

        if card_type == b'\x44\x00': # MIFARE UltraLight
            #raise NotImplementedError, "UltraLight card selected - no functions available"
            # HACK by TPG: The response format isn't fully known yet (official driver reads 7 bytes from offset +9 in the raw response)
            # - This code effectively reads all bytes after +9 (AA, BB, len, and three skilled by `send_packet`, then three skipped here)
            # - Official driver has `AA` bytes followed by `00` (encoded/decoded)
            command = b'\x12\x02'
            serial = self.send_packet(command)[3:]
            capacity = 0
            return (serial, capacity)
        else:
        # Otherwise, must be a standard MIFARE card.
            # Anticollision
            command = b'\x02\x02\x04'
            # No error handling on this command
            serial = self.send_packet(command)[3:]

            # Select the card for use
            try:
                select_response = self.send_packet(b'\x03\x02' + serial)
                capacity = select_response[3]
            except IndexError:
                logging.warning('Tried to select card but failed: card_type %s, serial %s, select_response %s' % (card_type.__repr__(), serial.__repr__(), select_response.__repr__()))
                capacity = 0
            return (serial, capacity)

    def sector_login(self, blockvect, key: bytes, keytype=0):
        """Log in to a block using the six-byte key.

        Use a keytype of 1 to use key B."""
        sector = self.get_absolute_block((blockvect[0], 0))

        if len(key) != 6:
            raise ValueError('key must be a six-byte string')

        keytype = 96 + keytype

        data = bytes([keytype, sector]) + key

        result = self.send_packet(b'\x07\x02' + data)
        if result[2] == 22:
            raise MIFAREAuthenticationException("incorrect key provided")

        return

    def read_block(self, blockvect):
        "Read the 16-byte block at vector (sector, block)."
        block = self.get_absolute_block(blockvect)

        result = self.send_packet( bytes([0x08,0x02, block]) )
        return result[3:19]

    def write_block(self, blockvect, data: bytes):
        """Write the 16 bytes in data to the block at vector (sector, block)."""
        block = self.get_absolute_block(blockvect)
        if len(data) != 16:
            raise ValueError("invalid data length - must be 16 bytes")

        result = self.send_packet( bytes([0x09,0x02,block]) + data )
        return #result[3:19]

    def write_key(self, key):
        pass

    def value_block_increment(self, blocknum, increment):
        pass

    def value_block_decrement(self, blocknum, decrement):
        pass

    def copy_block(self, source, dest):
        pass

    def halt(self):
        """Halt the current card - no further transactions will be performed with it."""
        self.send_packet(b'\x04\x02')

    def set_led(self, red = False, green = False):
        led_state = 0
        if red:
            led_state += 1
        if green:
            led_state += 2
        self.send_packet(bytes([0x07,0x01, led_state]))

    def beep(self, length):
        '''Beep for a specified length of milliseconds.'''
        length = int(round(length / 10.))
        if length > 255:
            length = 255
        self.send_packet(bytes([0x06,0x01, length]))

    def reset(self):
        pass
