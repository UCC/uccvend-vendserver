#!/usr/bin/env python

import string
import sys
import time

class HorizScroll:
	def __init__(self, text):
		self.text = text
		pass

	def expand(self, padding=None, paddingchar=" ", dir=None, wraparound=False):
		if len(self.text) <= 10:
			return [self.text]

		if padding == None:
			padding = len(self.text) // 2 + 1

		pad = paddingchar * padding
		padtext = self.text + pad
		if not wraparound:
			numiters = len(self.text) - 10
		else:
			numiters = len(padtext)

		expansion = []

		for x in range(0,numiters):
			expansion.append("%-10.10s" % (padtext[x:] + padtext[:x]))
		
		if dir == -1:
			expansion.reverse()

		return expansion

if __name__ == '__main__':
	h = HorizScroll("hello cruel world")
	eh = h.expand()
	while 1:
		for x in eh:
			sys.stdout.write("\r")
			print("%-10.10s" % x, end=' ')
			sys.stdout.flush()
			time.sleep(0.1)

