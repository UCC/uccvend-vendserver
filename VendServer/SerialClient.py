import os, termios
from time import sleep
import logging
from serial import *

class SerialClientException(Exception): pass

class SerialClient:
	def __init__(self, port = '/dev/ttyS1', baud = 9600):
		self.ser = Serial(
			port = port,
			baudrate = baud,
			bytesize=EIGHTBITS,     #number of databits
			parity=PARITY_NONE,     #enable parity checking
			stopbits=STOPBITS_ONE,  #number of stopbits
			timeout=10,           #set a timeout value, None for waiting forever, return on read
			xonxoff=0,              #enable software flow control
			rtscts=0,               #enable RTS/CTS flow control
		)
	

		self.rfh = ReadWrapper(self.ser)
		self.wfh = WriteWrapper(self.ser)
		self.wfh.write('B\n')

	def get_fh(self):
		return (self.rfh, self.wfh)

	def __del__(self):
	    pass
class WriteWrapper:
    def __init__(self, fh):
        self.fh = fh
    def write(self, s: str):
        return self.fh.write(s.encode('utf-8'))
    def flush(self):
        return self.fh.flush()
class ReadWrapper:
    def __init__(self, fh):
        self.fh = fh
    def fileno(self):
        return self.fh.fileno()
    def read(self, count) -> str:
        return self.fh.read(count).decode('utf-8')
    def readline(self) -> str:
        return self.fh.readline().decode('utf-8')


if __name__ == '__main__':
	s = SerialClient("/dev/ttyS1", 9600)
	
	(rfh, wfh) = s.get_fh()

	wfh.write('B\n')
	print(rfh.read())


