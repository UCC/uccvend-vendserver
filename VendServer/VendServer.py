#!/usr/bin/python
# vim: ts=4 sts=4 sw=4 noexpandtab

USE_MIFARE = 1

import configparser
import sys, os, string, re, pwd, signal, math, syslog
import logging, logging.handlers
from traceback import format_tb
from time import time, sleep, mktime, localtime
#from .LATClient import LATClient, LATClientException
from .SerialClient import SerialClient, SerialClientException
from .VendingMachine import VendingMachine, VendingException
from .MessageKeeper import MessageKeeper
from .HorizScroll import HorizScroll
from random import random, seed
from .Idler import Idler,GreetingIdler,TrainIdler,GrayIdler,StringIdler,ClockIdler,FortuneIdler,FileIdler,PipeIdler
import socket
from posix import geteuid
from .OpenDispense import OpenDispense as Dispense
try:
    import TracebackPrinter
except ImportError:
    from . import TracebackPrinter

CREDITS="""
This vending machine software brought to you by:
Bernard Blackham
Mark Tearle
Nick Bannon
Cameron Patrick
John Hodge
and a collective of hungry alpacas.

The MIFARE card reader bought to you by:
David Adam

Bug Hunting and hardware maintenance by:
Mitchell Pomery

For a good time call +61 8 6488 3901



"""

PIN_LENGTH = 4

DOOR = 1
SWITCH = 2
KEY = 3
TICK = 4
MIFARE = 5


(
STATE_IDLE,
STATE_DOOR_OPENING,
STATE_DOOR_CLOSING,
STATE_GETTING_UID,
STATE_GETTING_PIN,
STATE_GET_SELECTION,
STATE_GRANDFATHER_CLOCK,
) = list(range(1,8))

TEXT_SPEED = 0.8
IDLE_SPEED = 0.05


config_options = {
	'DBServer': ('Database', 'Server'),
	'DBName': ('Database', 'Name'),
	'DBUser': ('VendingMachine', 'DBUser'),
	'DBPassword': ('VendingMachine', 'DBPassword'),
	
	'ServiceName': ('VendingMachine', 'ServiceName'),
	'ServicePassword': ('VendingMachine', 'Password'),
	
	'ServerName': ('DecServer', 'Name'),
	'ConnectPassword': ('DecServer', 'ConnectPassword'),
	'PrivPassword': ('DecServer', 'PrivPassword'),
	}

class VendConfigFile:
	def __init__(self, config_file, options):
		try:
			cp = configparser.ConfigParser()
			cp.read(config_file)

			for option in options:
				section, name = options[option]
				value = cp.get(section, name)
				self.__dict__[option] = value
		
		except configparser.Error as e:
			raise SystemExit("Error reading config file "+config_file+": " + str(e))

"""
This class manages the current state of the vending machine.
"""
class VendState:
	def __init__(self,v):
		self.state_table = {}
		self.state = STATE_IDLE
		self.counter = 0

		self.mk = MessageKeeper(v)
		self.cur_user = ''
		self.cur_pin = ''
		self.username = ''
		self.cur_selection = ''
		self.time_to_autologout = None

		self.last_timeout_refresh = None

	def change_state(self,newstate,newcounter=None):
		if self.state != newstate:
			self.state = newstate

		if newcounter is not None and self.counter != newcounter:
			self.counter = newcounter

class VendServer():

	v = None
	vstatus = None
	state = None
	event = None
	idlers: list[Idler] = []
	idler = None

	_pin_uid = 0
	_pin_uname = 'root'
	_pin_pin = '----'
	_last_card_id = -1

	dispense = None

	"""
	Show information to the user as to what can be dispensed.
	"""
	def scroll_options(self, username, mk, welcome = False):
		# If the user has just logged in, show them their balance
		if welcome:
			balance = self.dispense.getBalance()
			msg = [(self.center('WELCOME'), False, TEXT_SPEED),
				   (self.center(self.dispense.getUsername()), False, TEXT_SPEED),
				   (self.center(balance), False, TEXT_SPEED),]
		else:
			msg = []
		choices = ' '*10+'CHOICES: '

		# Show what is in the coke machine
		# Need to update this so it uses the abstracted system
		cokes = []
		for i in ['08', '18', '28', '38', '48', '58', '68']:
			cokes.append((i, self.dispense.getItemInfo(i)))

		for c in cokes:
			if c[1][0] == 'dead':
				continue
			choices += '%s-(%sc)-%s8 '%(c[1][0], c[1][1], c[0])

		# Show the final few options
		choices += '55-DOOR '
		choices += 'OR ANOTHER SNACK. '
		choices += '99 TO READ AGAIN. '
		choices += 'CHOICE?   '
		msg.append((choices, False, None))
		# Send it to the display
		mk.set_messages(msg)

	"""
	In here just for fun.
	"""
	def cookie(self):
		seed(time())
		messages = ['  WASSUP! ', 'PINK FISH ', ' SECRETS ', '  ESKIMO  ', ' FORTUNES ', 'MORE MONEY']
		choice = int(random()*len(messages))
		msg = messages[choice]
		left = list(range(len(msg)))
		for i in range(len(msg)):
			if msg[i] == ' ': left.remove(i)
		reveal = 1
		while left:
			s = ''
			for i in range(0, len(msg)):
				if i in left:
					if reveal == 0:
						left.remove(i)
						s += msg[i]
					else:
						s += chr(int(random()*26)+ord('A'))
					reveal += 1
					reveal %= 17
				else:
					s += msg[i]
			self.v.display(s)

	"""
	Format text so it will appear centered on the screen.
	"""
	def center(self, str: str):
		LEN = 10
		return ' '*((LEN-len(str))//2)+str

	"""
	Configure the things that will appear on screen whil the machine is idling.
	"""
	def setup_idlers(self):
		self.idlers = [
			#
			GrayIdler(self.v),
			GrayIdler(self.v,one="*",zero="-"),
			GrayIdler(self.v,one="/",zero="\\"),
			GrayIdler(self.v,one="X",zero="O"),
			GrayIdler(self.v,one="*",zero="-",reorder=1),
			GrayIdler(self.v,one="/",zero="\\",reorder=1),
			GrayIdler(self.v,one="X",zero="O",reorder=1),
			#
			ClockIdler(self.v),
			ClockIdler(self.v),
			ClockIdler(self.v),
			#
			StringIdler(self.v), # Hello Cruel World
			StringIdler(self.v, text="Kill 'em all", repeat=False),
			StringIdler(self.v, text=CREDITS),
			StringIdler(self.v, text=str(math.pi) + "            "),
			StringIdler(self.v, text=str(math.e) + "            "),
			StringIdler(self.v, text="    I want some pizza - please call Broadway Pizza on +61 8 9389 8500 - and order as Quinn - I am getting really hungry", repeat=False),
			# "Hello World" in brainfuck
			StringIdler(self.v, text=">+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-] <.>+++++++++++[<++++++++>-]<-.--------.+++.------.--------.[-]>++++++++[<++++>- ]<+.[-]++++++++++."),
			#
			TrainIdler(self.v),
			#
			FileIdler(self.v, '/usr/share/common-licenses/GPL-2',affinity=2),
			#
			PipeIdler(self.v, "/usr/bin/getent", "passwd"),
			FortuneIdler(self.v,affinity=20),
			]
		disabled = [
			]

	"""
	Go back to the default idler.
	"""
	def reset_idler(self, t = None):
		self.idler = GreetingIdler(self.v, t)
		self.vstatus.time_of_next_idlestep = time()+next(self.idler)
		self.vstatus.time_of_next_idler = None
		self.vstatus.time_to_autologout = None
		self.vstatus.change_state(STATE_IDLE, 1)

	"""
	Change to a random idler.
	"""
	def choose_idler(self):

		# Implementation of the King Of the Hill algorithm from;
		# http://eli.thegreenplace.net/2010/01/22/weighted-random-generation-in-python/

		#def weighted_choice_king(weights):
		#	total = 0
		#	winner = 0
		#	for i, w in enumerate(weights):
		#		total += w
		#		if random.random() * total < w:
		#			winner = i
		#	return winner
		#	

		total = 0
		winner = None
		
		for choice in self.idlers:
			weight = choice.affinity()
			total += weight
			if random() * total < weight:
				winner = choice

		self.idler = winner

		if self.idler:
			self.idler.reset()
	"""
	Run every step while the machine is idling.
	"""
	def idle_step(self):
		if self.idler.finished():
			self.choose_idler()
			self.vstatus.time_of_next_idler = time() + 30
		nextidle = next(self.idler)
		if nextidle is None:
			nextidle = IDLE_SPEED
		self.vstatus.time_of_next_idlestep = time()+nextidle

	"""
	These next two events trigger no response in the code.
	"""
	def handle_tick_event(self, event, params):
		# don't care right now.
		pass

	def handle_switch_event(self, event, params):
		# don't care right now.
		pass

	"""
	Don't do anything for this event.
	"""
	def do_nothing(self, event, params):
		print("doing nothing (s,e,p)", self.state, " ", event, " ", params)
		pass

	"""
	These next few entrie tell us to do nothing while we are idling
	"""
	def handle_getting_uid_idle(self, event, params):
		# don't care right now.
		pass

	def handle_getting_pin_idle(self, event, params):
		# don't care right now.
		pass

	"""
	While logged in and waiting for user input, slowly get closer to logging out.
	"""
	def handle_get_selection_idle(self, event, params):
		# don't care right now.
		###
		### State logging out ..
		if self.vstatus.time_to_autologout != None:
			time_left = self.vstatus.time_to_autologout - time()
			if time_left < 6 and (self.vstatus.last_timeout_refresh is None or self.vstatus.last_timeout_refresh > time_left):
				self.vstatus.mk.set_message('LOGOUT: '+str(int(time_left)))
				self.vstatus.last_timeout_refresh = int(time_left)
				self.vstatus.cur_selection = ''

		# Login timed out: Log out the current user.
		if self.vstatus.time_to_autologout != None and self.vstatus.time_to_autologout - time() <= 0:
			self.vstatus.time_to_autologout = None
			self.vstatus.cur_user = ''
			self.vstatus.cur_pin = ''
			self.vstatus.cur_selection = ''
			self._last_card_id = -1
			self.dispense.logOut()
			self.reset_idler()

		### State fully logged out ... reset variables
		if self.vstatus.time_to_autologout and not self.vstatus.mk.done(): 
			self.vstatus.time_to_autologout = None
		if self.vstatus.cur_user == '' and self.vstatus.time_to_autologout: 
			self.vstatus.time_to_autologout = None
		
		### State logged in
		if len(self.vstatus.cur_pin) == PIN_LENGTH and self.vstatus.mk.done() and self.vstatus.time_to_autologout == None:
			# start autologout
			self.vstatus.time_to_autologout = time() + 15
			self.vstatus.last_timeout_refresh = None

		## FIXME - this may need to be elsewhere.....
		# need to check
		self.vstatus.mk.update_display()

	"""
	Triggered on user input while logged in.
	"""
	def handle_get_selection_key(self, event, params):
		key = params
		if len(self.vstatus.cur_selection) == 0:
			if key == 11:
				self.vstatus.cur_pin = ''
				self.vstatus.cur_user = ''
				self.vstatus.cur_selection = ''
				self._last_card_id = -1
				self.dispense.logOut()
				self.vstatus.mk.set_messages([(self.center('BYE!'), False, 1.5)])
				self.reset_idler(2)
				return
			self.vstatus.cur_selection += chr(key + ord('0'))
			self.vstatus.mk.set_message('SELECT: '+self.vstatus.cur_selection)
			self.vstatus.time_to_autologout = None
		elif len(self.vstatus.cur_selection) == 1:
			if key == 11:
				self.vstatus.cur_selection = ''
				self.vstatus.time_to_autologout = None
				self.dispense.logOut()
				self.scroll_options(self.vstatus.username, self.vstatus.mk)
				return
			else:
				self.vstatus.cur_selection += chr(key + ord('0'))
				if self.dispense.isLoggedIn():
					self.make_selection()
					self.vstatus.cur_selection = ''
					self.vstatus.time_to_autologout = time() + 8
					self.vstatus.last_timeout_refresh = None
				else:
					# Price check mode.
					(name,price) = self.dispense.getItemInfo(self.vstatus.cur_selection)
					dollarprice = "$%.2f" % ( price / 100.0 )
					self.v.display( self.vstatus.cur_selection+' - %s'%dollarprice)

					self.vstatus.cur_selection = ''
					self.vstatus.time_to_autologout = None
					self.vstatus.last_timeout_refresh = None

	"""
	Triggered when the user has entered the id of something they would like to purchase.
	"""
	def make_selection(self):
		logging.debug('Dispense item "%s"' % (self.vstatus.cur_selection,))
		# should use sudo here
		if self.vstatus.cur_selection == '55':
			self.vstatus.mk.set_message('OPENSESAME')
			logging.info('dispensing a door for %s'%self.vstatus.username)
			if self.dispense.openDoor():
				logging.info('door opened')
				self.vstatus.mk.set_message(self.center('DOOR OPEN'))
			else:
				logging.warning('user %s tried to dispense a bad door'%self.vstatus.username)
				self.vstatus.mk.set_message(self.center('BAD DOOR'))
			sleep(1)
		elif self.vstatus.cur_selection == '81':
			self.cookie()
		elif self.vstatus.cur_selection == '99':
			self.scroll_options(self.vstatus.username, self.vstatus.mk)
			self.vstatus.cur_selection = ''
			return
		elif self.vstatus.cur_selection[1] == '8':
			# Drinks: No need to vend, just print a funny message and ask the server to vend from coke
			logging.info('dispensing drink {} for {}'.format(self.vstatus.cur_selection, self.vstatus.username))
			self.v.display('GOT DRINK?')
			if self.dispense.dispenseItem(self.vstatus.cur_selection) != "200":
				self.v.display('SEEMS NOT')
			else:
				self.v.display('GOT DRINK!')
		else:
			logging.info('dispensing snack {} for {}'.format(self.vstatus.cur_selection, self.vstatus.username))
			# Snacks: Show the name/price then dispense it
			name, price = self.dispense.getItemInfo( self.vstatus.cur_selection )
			dollarprice = "$%.2f" % ( price / 100.0 )
			self.v.display(self.vstatus.cur_selection+' - %s'%dollarprice)
			status = self.dispense.dispenseItem(self.vstatus.cur_selection)
			if status == "200":
				# magic dispense syslog service
				(worked, code, string) = self.v.vend(self.vstatus.cur_selection)
				if worked:
					self.v.display('THANK YOU')
					syslog.syslog(syslog.LOG_INFO | syslog.LOG_LOCAL4, "vended %s (slot %s) for %s" % (name, self.vstatus.cur_selection, self.vstatus.username))
				else:
					print("Vend Failed:", code, string)
					syslog.syslog(syslog.LOG_WARNING | syslog.LOG_LOCAL4, "vending %s (slot %s) for %s FAILED %r %r" % (name, self.vstatus.cur_selection, self.vstatus.username, code, string))
					self.v.display('VEND FAIL')
			elif status == "402":	# RV_BALANCE
				self.v.display('NO MONEY?')
			elif status == "406":	# RV_BADITEM (Dead slot)
				self.v.display('EMPTY SLOT')
			else:
				syslog.syslog(syslog.LOG_INFO | syslog.LOG_LOCAL4, "failed vending %s (slot %s) for %s (code %s)" % (name, self.vstatus.cur_selection, self.vstatus.username, status))
				self.v.display('UNK ERROR')
		sleep(1)

	"""
	Triggered when the user presses a button while entering their pin.
	"""
	def handle_getting_pin_key(self, event, params):
		key = params
		if len(self.vstatus.cur_pin) < PIN_LENGTH:
			if key == 11:
				if self.vstatus.cur_pin == '':
					self.vstatus.cur_user = ''
					self.dispense.logOut()
					self.reset_idler()

					return
				self.vstatus.cur_pin = ''
				self.vstatus.mk.set_message('PIN: ')
				return
			self.vstatus.cur_pin += chr(key + ord('0'))
			self.vstatus.mk.set_message('PIN: '+'X'*len(self.vstatus.cur_pin))
			if len(self.vstatus.cur_pin) == PIN_LENGTH:
				if self.dispense.authUserIdPin(self.vstatus.cur_user, self.vstatus.cur_pin):
					self.vstatus.username = self.dispense.getUsername()
					self.v.beep(0, False)
					self.vstatus.cur_selection = ''
					self.vstatus.change_state(STATE_GET_SELECTION)
					self.scroll_options(self.vstatus.username, self.vstatus.mk, True)
					return
				else:
					self.v.beep(40, False)
					self.vstatus.mk.set_messages(
						[(self.center('BAD PIN'), False, 1.0),
						 (self.center('SORRY'), False, 0.5)])
					self.vstatus.cur_user = ''
					self.vstatus.cur_pin = ''
				
					self.reset_idler(2)

					return

	"""
	Triggered when the user presses a button while entering their user id.
	"""
	def handle_getting_uid_key(self, event, params):
		key = params
		# complicated key handling here:

		if len(self.vstatus.cur_user) == 0 and key == 9:
			self.vstatus.cur_selection = ''
			self.vstatus.time_to_autologout = None
			self.vstatus.mk.set_message('PRICECHECK')
			sleep(0.5)
			self.scroll_options('', self.vstatus.mk)
			self.vstatus.change_state(STATE_GET_SELECTION)
			return

		if len(self.vstatus.cur_user) <8:
			if key == 11:
				self.vstatus.cur_user = ''
				self.dispense.logOut()

				self.reset_idler()
				return
			self.vstatus.cur_user += chr(key + ord('0'))
			#logging.info('dob: '+vstatus.cur_user)
			if len(self.vstatus.cur_user) > 5:
				self.vstatus.mk.set_message('>'+self.vstatus.cur_user)
			else:
				self.vstatus.mk.set_message('UID: '+self.vstatus.cur_user)
		
		if len(self.vstatus.cur_user) == 5:
			uid = int(self.vstatus.cur_user)

			if uid == 0:
				logging.info('user '+self.vstatus.cur_user+' has a bad PIN')
				pfalken="""
	CARRIER DETECTED

	CONNECT 128000

	Welcome to Picklevision Sytems, Sunnyvale, CA

	Greetings Professor Falken.




	Shall we play a game?


	Please choose from the following menu:

	1. Tic-Tac-Toe
	2. Chess
	3. Checkers
	4. Backgammon
	5. Poker
	6. Toxic and Biochemical Warfare
	7. Global Thermonuclear War

	7 [ENTER]

	Wouldn't you prefer a nice game of chess?

	""".replace('\n','    ')
				self.vstatus.mk.set_messages([(pfalken, False, 10)])
				self.vstatus.cur_user = ''
				self.vstatus.cur_pin = ''
				
				self.reset_idler(10)

				return

			# TODO Fix this up do we can check before logging in
			"""
			if self.dispense.isDisabled():
				logging.info('user '+self.vstatus.cur_user+' is disabled')
				self.vstatus.mk.set_messages(
					[(' '*11+'ACCOUNT DISABLED'+' '*11, False, 3)])
				self.vstatus.cur_user = ''
				self.vstatus.cur_pin = ''
				
				self.reset_idler(3)
				return
			"""

			self.vstatus.cur_pin = ''
			self.vstatus.mk.set_message('PIN: ')
			logging.info('need pin for user %s'%self.vstatus.cur_user)
			self.vstatus.change_state(STATE_GETTING_PIN)
			return

	"""
	Triggered when a key is pressed and the machine is idling.
	"""
	def handle_idle_key(self, event, params):
		key = params
		if key == 11:
			self.vstatus.cur_user = ''
			self.dispense.logOut()
			self.reset_idler()
			return
		
		self.vstatus.change_state(STATE_GETTING_UID)
		self.run_handler(event, params)

	"""
	What to do when there is nothing to do.
	"""
	def handle_idle_tick(self, event, params):
		### State idling
		if self.vstatus.mk.done():
			self.idle_step()

		if self.vstatus.time_of_next_idler and time() > self.vstatus.time_of_next_idler:
			self.vstatus.time_of_next_idler = time() + 30
			self.choose_idler()
		
		###

		self.vstatus.mk.update_display()

		self.vstatus.change_state(STATE_GRANDFATHER_CLOCK)
		self.run_handler(event, params)
		sleep(0.05)

	"""
	Manages the beeps for the grandfather clock
	"""
	def beep_on(self, when, before=0):
		start = int(when - before)
		end = int(when)
		now = int(time())

		if now >= start and now <= end:
			return 1
		return 0

	def handle_idle_grandfather_tick(self, event, params):
		### check for interesting times
		now = localtime()

		quarterhour      = mktime((now[0],now[1],now[2],now[3],15,0,now[6],now[7],now[8]))
		halfhour         = mktime((now[0],now[1],now[2],now[3],30,0,now[6],now[7],now[8]))
		threequarterhour = mktime((now[0],now[1],now[2],now[3],45,0,now[6],now[7],now[8]))
		fivetothehour    = mktime((now[0],now[1],now[2],now[3],55,0,now[6],now[7],now[8]))

		hourfromnow = localtime(time() + 3600)
		
		#onthehour = mktime([now[0],now[1],now[2],now[3],03,0,now[6],now[7],now[8]])
		onthehour = mktime((hourfromnow[0],hourfromnow[1],hourfromnow[2],hourfromnow[3], \
			0,0,hourfromnow[6],hourfromnow[7],hourfromnow[8]))

		## check for X seconds to the hour
		## if case, update counter to 2
		if self.beep_on(onthehour,15) \
			or self.beep_on(halfhour,0) \
			or self.beep_on(quarterhour,0) \
			or self.beep_on(threequarterhour,0) \
			or self.beep_on(fivetothehour,0):
			self.vstatus.change_state(STATE_GRANDFATHER_CLOCK,2)
			self.run_handler(event, params)
		else:
			self.vstatus.change_state(STATE_IDLE)

	def handle_grandfather_tick(self, event, params):
		go_idle = 1

		msg = []
		### we live in interesting times
		now = localtime()

		quarterhour      = mktime((now[0],now[1],now[2],now[3],15,0,now[6],now[7],now[8]))
		halfhour         = mktime((now[0],now[1],now[2],now[3],30,0,now[6],now[7],now[8]))
		threequarterhour = mktime((now[0],now[1],now[2],now[3],45,0,now[6],now[7],now[8]))
		fivetothehour    = mktime((now[0],now[1],now[2],now[3],55,0,now[6],now[7],now[8]))

		hourfromnow = localtime(time() + 3600)
		
	#	onthehour = mktime([now[0],now[1],now[2],now[3],03,0,now[6],now[7],now[8]])
		onthehour = mktime((hourfromnow[0],hourfromnow[1],hourfromnow[2],hourfromnow[3], \
			0,0,hourfromnow[6],hourfromnow[7],hourfromnow[8]))


		#print "when it fashionable to wear a onion on your hip"

		if self.beep_on(onthehour,15):
			go_idle = 0
			next_hour=((hourfromnow[3] + 11) % 12) + 1
			if onthehour - time() < next_hour and onthehour - time() > 0:
				self.v.beep(0, False)

				t = int(time())
				if (t % 2) == 0:
					msg.append(("DING!", False, None))
				else:
					msg.append(("     DING!", False, None))
			elif int(onthehour - time()) == 0:
				self.v.beep(255, False)
				msg.append(("   BONG!", False, None))
				msg.append(("     IT'S "+ str(next_hour) + "O'CLOCK AND ALL IS WELL .....", False, TEXT_SPEED*4))
		elif self.beep_on(halfhour,0):
			go_idle = 0
			self.v.beep(0, False)
			msg.append((" HALFHOUR ", False, 50))
		elif self.beep_on(quarterhour,0):
			go_idle = 0
			self.v.beep(0, False)
			msg.append((" QTR HOUR ", False, 50))
		elif self.beep_on(threequarterhour,0):
			go_idle = 0
			self.v.beep(0, False)
			msg.append((" 3 QTR HR ", False, 50))
		elif self.beep_on(fivetothehour,0):
			go_idle = 0
			self.v.beep(0, False)
			msg.append(("Quick run to your lectures!  Hurry! Hurry!", False, TEXT_SPEED*4))
		else:
			go_idle = 1
		
		## check for X seconds to the hour

		if len(msg):
			self.vstatus.mk.set_messages(msg)
			sleep(1)

		self.vstatus.mk.update_display()
		## if no longer case, return to idle

		## change idler to be clock
		if go_idle and self.vstatus.mk.done():
			self.vstatus.change_state(STATE_IDLE,1)

	"""
	What to do when the door is open.
	"""
	def handle_door_idle(self, event, params):
		def twiddle(clock,v,wise = 2):
			if (clock % 4 == 0):
				v.display("-FEED  ME-")
			elif (clock % 4 == 1+wise):
				v.display("\\FEED  ME/")
			elif (clock % 4 == 2):
				v.display("-FEED  ME-")
			elif (clock % 4 == 3-wise):
				v.display("/FEED  ME\\")

		# don't care right now.
		now = int(time())

		if ((now % 60 % 2) == 0):
			twiddle(now, self.v)
		else:
			twiddle(now, self.v, wise=0)

	"""
	What to do when the door is opened or closed.
	"""
	def handle_door_event(self, event, params):
		if params == 0:  #door open
			self.vstatus.change_state(STATE_DOOR_OPENING)
			logging.warning("Entering open door mode")
			self.v.display("-FEED  ME-")
			#door_open_mode(v);
			self.dispense.logOut()
			self.vstatus.cur_user = ''
			self.vstatus.cur_pin = ''
		elif params == 1:  #door closed
			self.vstatus.change_state(STATE_DOOR_CLOSING)
			self.reset_idler(3)

			logging.warning('Leaving open door mode')
			self.v.display("-YUM YUM!-")

	"""
	Triggered when a user swipes their caed, and the machine is logged out.
	"""
	def handle_mifare_event(self, event, params):
		card_id = params
		# Translate card_id into uid.
		if card_id == None or card_id == self._last_card_id:
			return

		self._last_card_id = card_id
		
		if not self.dispense.authMifareCard(card_id):
			self.v.beep(40, False)
			self.vstatus.mk.set_messages(
				[(self.center('BAD CARD'), False, 1.0),
				 (self.center('SORRY'), False, 0.5)])
			self.vstatus.cur_user = ''
			self.vstatus.cur_pin = ''
			self._last_card_id = -1
		
			self.reset_idler(2)
			return
		elif self.dispense.isDisabled():
			logging.info('Mapped card id to uid %s'%self.dispense.getUsername())
			self.v.beep(40, False)
			self.vstatus.mk.set_messages(
				[(self.center('ACCT DISABLED'), False, 1.0),
				 (self.center('SORRY'), False, 0.5)])
			self.dispense.logOut()
			self.reset_idler(2)
			return
		else:
			logging.info('Mapped card id to uid %s'%self.dispense.getUsername())
			self.vstatus.cur_user = '----'
			self.vstatus.username = self.dispense.getUsername()
			self.vstatus.cur_selection = ''
			self.vstatus.change_state(STATE_GET_SELECTION)
			self.scroll_options(self.vstatus.username, self.vstatus.mk, True)
			return

	"""
	Triggered when a user swipes their card and the machine is logged in.
	"""
	def handle_mifare_add_user_event(self, event, params):
		card_id = params

		# Translate card_id into uid.
		if card_id == None or card_id == self._last_card_id:
			return

		self._last_card_id = card_id

		if not self.dispense.addCard(card_id):
			self.vstatus.mk.set_messages(
				[(self.center('ALREADY'), False, 0.5),
				 (self.center('ENROLLED'), False, 0.5)])
		else:
			self.vstatus.mk.set_messages(
				[(self.center('CARD'), False, 0.5),
				 (self.center('ENROLLED'), False, 0.5)])

	def return_to_idle(self, event, params):
		self.reset_idler()

	"""
	Maps what to do when the state changes.
	"""
	def create_state_table(self):
		self.vstatus.state_table[(STATE_IDLE,TICK,1)] = self.handle_idle_tick
		self.vstatus.state_table[(STATE_IDLE,KEY,1)] = self.handle_idle_key
		self.vstatus.state_table[(STATE_IDLE,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_IDLE,MIFARE,1)] = self.handle_mifare_event

		self.vstatus.state_table[(STATE_DOOR_OPENING,TICK,1)] = self.handle_door_idle
		self.vstatus.state_table[(STATE_DOOR_OPENING,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_DOOR_OPENING,KEY,1)] = self.do_nothing
		self.vstatus.state_table[(STATE_DOOR_OPENING,MIFARE,1)] = self.do_nothing

		self.vstatus.state_table[(STATE_DOOR_CLOSING,TICK,1)] = self.return_to_idle
		self.vstatus.state_table[(STATE_DOOR_CLOSING,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_DOOR_CLOSING,KEY,1)] = self.do_nothing
		self.vstatus.state_table[(STATE_DOOR_CLOSING,MIFARE,1)] = self.do_nothing

		self.vstatus.state_table[(STATE_GETTING_UID,TICK,1)] = self.handle_getting_uid_idle
		self.vstatus.state_table[(STATE_GETTING_UID,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_GETTING_UID,KEY,1)] = self.handle_getting_uid_key
		self.vstatus.state_table[(STATE_GETTING_UID,MIFARE,1)] = self.handle_mifare_event

		self.vstatus.state_table[(STATE_GETTING_PIN,TICK,1)] = self.handle_getting_pin_idle
		self.vstatus.state_table[(STATE_GETTING_PIN,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_GETTING_PIN,KEY,1)] = self.handle_getting_pin_key
		self.vstatus.state_table[(STATE_GETTING_PIN,MIFARE,1)] = self.handle_mifare_event

		self.vstatus.state_table[(STATE_GET_SELECTION,TICK,1)] = self.handle_get_selection_idle
		self.vstatus.state_table[(STATE_GET_SELECTION,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_GET_SELECTION,KEY,1)] = self.handle_get_selection_key
		self.vstatus.state_table[(STATE_GET_SELECTION,MIFARE,1)] = self.handle_mifare_add_user_event

		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,TICK,1)] = self.handle_idle_grandfather_tick
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,TICK,2)] = self.handle_grandfather_tick
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,DOOR,1)] = self.handle_door_event
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,DOOR,2)] = self.handle_door_event
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,KEY,1)] = self.do_nothing
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,KEY,2)] = self.do_nothing
		self.vstatus.state_table[(STATE_GRANDFATHER_CLOCK,MIFARE,1)] = self.handle_mifare_event

	"""
	Get what to do on a state change.
	"""
	def get_state_table_handler(self, state, event, counter):
		return self.vstatus.state_table[(state,event,counter)]

	def time_to_next_update(self):
		idle_update = self.vstatus.time_of_next_idlestep - time()
		if not self.vstatus.mk.done() and self.vstatus.mk.next_update is not None:
			mk_update = self.vstatus.mk.next_update - time()
			if mk_update < idle_update:
				idle_update = mk_update
		return idle_update

	def run_forever(self, rfh, wfh, options, cf):
		self.v = VendingMachine(rfh, wfh, USE_MIFARE)
		self.dispense = Dispense()
		self.vstatus = VendState(self.v)
		self.create_state_table()

		logging.debug('PING is ' + str(self.v.ping()))

		self.setup_idlers()
		self.reset_idler()

		while True:
			timeout = self.time_to_next_update()
			(event, params) = self.v.next_event(timeout)
			self.run_handler(event, params)

	def run_handler(self, event, params):
		handler = self.get_state_table_handler(self.vstatus.state,event,self.vstatus.counter)
		if handler:
			handler(event, params)

"""
Connect to the machine.
"""
def connect_to_vend(options, cf):

	if False and options.use_lat:
		logging.info('Connecting to vending machine using LAT')
		latclient = LATClient(service = cf.ServiceName, password = cf.ServicePassword, server_name = cf.ServerName, connect_password = cf.ConnectPassword, priv_password = cf.PrivPassword)
		rfh, wfh = latclient.get_fh()
	elif options.use_serial:
		# Open vending machine via serial.
		logging.info('Connecting to vending machine using serial')
		serialclient = SerialClient(port = '/dev/ttyS1', baud = 9600)
		rfh,wfh = serialclient.get_fh()
	else:
		#(rfh, wfh) = popen2('../../virtualvend/vvend.py')
		logging.info('Connecting to virtual vending machine on %s:%d'%(options.host,options.port))
		import socket
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
		sock.connect((options.host, options.port))
		rfh = sock.makefile('r')
		wfh = sock.makefile('w')
		global USE_MIFARE
		USE_MIFARE = 0
		
	return rfh, wfh

"""
Parse arguments from the command line
"""
def parse_args():
	from optparse import OptionParser

	op = OptionParser(usage="%prog [OPTION]...")
	op.add_option('-f', '--config-file', default='/etc/dispense2/servers.conf', metavar='FILE', dest='config_file', help='use the specified config file instead of /etc/dispense/servers.conf')
	op.add_option('--serial', action='store_true', default=False, dest='use_serial', help='use the serial port')
	op.add_option('--lat', action='store_true', default=False, dest='use_lat', help='use LAT')
	op.add_option('--virtualvend', action='store_false', default=True, dest='use_serial', help='use the virtual vending server instead of LAT')
	op.add_option('-n', '--hostname', dest='host', default='localhost', help='the hostname to connect to for virtual vending machine mode (default: localhost)')
	op.add_option('-p', '--port', dest='port', default=5150, type='int', help='the port number to connect to (default: 5150)')
	op.add_option('-l', '--log-file', metavar='FILE', dest='log_file', default='', help='log output to the specified file')
	op.add_option('-s', '--syslog', dest='syslog', metavar='FACILITY', default=None, help='log output to given syslog facility')
	op.add_option('-d', '--daemon', dest='daemon', action='store_true', default=False, help='run as a daemon')
	op.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='spit out lots of debug output')
	op.add_option('-q', '--quiet', dest='quiet', action='store_true', default=False, help='only report errors')
	op.add_option('--pid-file', dest='pid_file', metavar='FILE', default='', help='store daemon\'s pid in the given file')
	op.add_option('--traceback-file', dest='traceback_file', default='', help='destination to print tracebacks when receiving SIGUSR1')
	op.add_option('--crash', action='store_true', help="Crash immediately instead of looping")
	options, args = op.parse_args()

	if len(args) != 0:
		op.error('extra command line arguments: ' + ' '.join(args))

	return options

def create_pid_file(name):
	try:
		pid_file = open(name, 'w')
		pid_file.write('%d\n'%os.getpid())
		pid_file.close()
	except IOError as e:
		logging.warning('unable to write to pid file '+name+': '+str(e))

def set_stuff_up():
	def do_nothing(signum, stack):
		signal.signal(signum, do_nothing)
	def stop_server(signum, stack): raise KeyboardInterrupt
	signal.signal(signal.SIGHUP, do_nothing)
	signal.signal(signal.SIGTERM, stop_server)
	signal.signal(signal.SIGINT, stop_server)

	options = parse_args()
	config_opts = VendConfigFile(options.config_file, config_options)
	if options.daemon: become_daemon()
	set_up_logging(options)
	if options.pid_file != '': create_pid_file(options.pid_file)
	if options.traceback_file != '': TracebackPrinter.traceback_init(options.traceback_file)
	return options, config_opts

def clean_up_nicely(options, config_opts):
	if options.pid_file != '':
		try:
			os.unlink(options.pid_file)
			logging.debug('Removed pid file '+options.pid_file)
		except OSError: pass  # if we can't delete it, meh

def set_up_logging(options):
	logger = logging.getLogger()
	
	if not options.daemon:
		stderr_logger = logging.StreamHandler(sys.stderr)
		stderr_logger.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
		logger.addHandler(stderr_logger)
	
	if options.log_file != '':
		try:
			file_logger = logging.FileHandler(options.log_file)
			file_logger.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
			logger.addHandler(file_logger)
		except IOError as e:
			logger.warning('unable to write to log file '+options.log_file+': '+str(e))

	if options.syslog != None:
		sys_logger = logging.handlers.SysLogHandler('/dev/log', options.syslog)
		sys_logger.setFormatter(logging.Formatter('vendserver[%d]'%(os.getpid()) + ' %(levelname)s: %(message)s'))
		logger.addHandler(sys_logger)

	if options.quiet:
		logger.setLevel(logging.WARNING)
	elif options.verbose:
		logger.setLevel(logging.DEBUG)
	else:
		logger.setLevel(logging.INFO)

def become_daemon():
	dev_null = open('/dev/null')
	fd = dev_null.fileno()
	os.dup2(fd, 0)
	os.dup2(fd, 1)
	os.dup2(fd, 2)
	try:
		if os.fork() != 0:
			sys.exit(0)
		os.setsid()
	except OSError as e:
		raise SystemExit('failed to fork: '+str(e))

def do_vend_server(options, config_opts):
	while True:
		try:
			rfh, wfh = connect_to_vend(options, config_opts)
		except (SerialClientException, socket.error) as e:
			(exc_type, exc_value, exc_traceback) = sys.exc_info()
			del exc_traceback
			logging.error("Connection error: "+str(exc_type)+" "+str(e))
			logging.info("Trying again in 5 seconds.")
			sleep(5)
			continue
		
#		run_forever(rfh, wfh, options, config_opts)

		try:
			vserver = VendServer()
			vserver.run_forever(rfh, wfh, options, config_opts)
		except VendingException:
			logging.error("Connection died, trying again...")
			logging.info("Trying again in 5 seconds.")
			sleep(5)


def main(argv=None):
	options, config_opts = set_stuff_up()
	while True:
		try:
			logging.warning('Starting Vend Server')
			do_vend_server(options, config_opts)
			logging.error('Vend Server finished unexpectedly, restarting')
		except KeyboardInterrupt:
			logging.info("Killed by signal, cleaning up")
			# NOTE: When debugging deadlocks, enable this
			if options.crash:
				(exc_type, exc_value, exc_traceback) = sys.exc_info()
				tb = format_tb(exc_traceback, 20)
				del exc_traceback
				logging.info("Traceback:")
				for event in tb:
					for line in event.strip().split('\n'):
						logging.critical('    '+line)
			clean_up_nicely(options, config_opts)
			logging.warning("Vend Server stopped")
			break
		except SystemExit:
			break
		except:
			if options.crash:
				raise
			(exc_type, exc_value, exc_traceback) = sys.exc_info()
			tb = format_tb(exc_traceback, 20)
			del exc_traceback
			
			logging.critical("Uh-oh, unhandled " + str(exc_type) + " exception")
			logging.critical("Message: " + str(exc_value))
			logging.critical("Traceback:")
			for event in tb:
				for line in event.strip().split('\n'):
					logging.critical('    '+line)
			logging.critical("This message should be considered a bug in the Vend Server.")
			logging.critical("Please report this to someone who can fix it.")
			sleep(10)
			logging.warning("Trying again anyway (might not help, but hey...)")

if __name__ == '__main__':
	sys.exit(main())
