"""
Author: Mitchell Pomery (bobgeorge33)

Dispense is an abstract class that allows easy configuration between different systems. Anything that is 
intended to talk to OpenDispense or similar should comply with this class. Using this class in particular 
will cause you a lot of problems.
"""

class DispenseInterface(object):

	"""
	Create a new dispense object.
	@param username The username to connect to dispense with
	@param secret The secret to use for Auth
	"""
	def __init__(self, username=None, secret=None):
		pass

	"""
	Create a new dispense interface as the supplied user.
	@param username The userid of the person authing to dispense
	@param pin Thier pin
	"""
	def authUsernamePin(self, userId, pin):
		pass

	"""
	Create a new dispense interface as the supplied user.
	@param cardId The card that someone is connecting to Dispense with
	"""
	def authMifare(self, cardId):
		pass

	"""
	Add a MIFARE card for this user
	@param cardId Card to add to a user account
	@return 1 if added successfully, anything else otherwise
	"""
	def addCard(self, cardId):
		pass

	"""
	Check if creating the user worked correctly.
	@return True if currently logged in as a user, false otherwise
	"""
	def isLoggedIn(self):
		pass

	"""
	Get the current users username.
	@return The username of the current user. An empty string if not currently logged in.
	"""
	def getUsername(self):
		pass

	"""
	Get the current users balance.
	@return the users balance. None if not logged in.
	"""
	def getBalance(self):
		pass

	"""
	Get the name and price of an item.
	@param itemId The number entered into the vending machine
	@return (itemname, price)
	"""
	def getItemInfo(self, itemId):
		pass

	"""
	Check if the user is disabled.
	@return True if the user is disabled
	"""
	def isDisabled(self):
		pass

	"""
	Dispense an item for the current user.
	@param itemId The number entered into the vending machine
	"""
	def dispenseItem(self, itemId):
		pass

	"""
	Log the current user out
	"""
	def logOut(self):
		pass


