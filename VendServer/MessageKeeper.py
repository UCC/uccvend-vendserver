#!/usr/bin/python
# vim:ts=4

import sys, os, string, re, pwd, signal
from .HorizScroll import HorizScroll
from random import random, seed
from time import time, sleep

class MessageKeeper:
	def __init__(self, vendie):
		# Each element of scrolling_message should be a 3-tuple of
		# ('message', True/False if it is to be repeated, time to display)
		self.scrolling_message = []
		self.v = vendie
		self.next_update = None

	def set_message(self, string):
		self.scrolling_message = [(string, False, None)]
		self.update_display(True)

	def set_messages(self, strings):
		self.scrolling_message = strings
		self.update_display(True)

	def update_display(self, forced = False, padding = 0):
		if not forced and self.next_update != None and time() < self.next_update:
			return
		if len(self.scrolling_message) > 0:
			if len(self.scrolling_message[0][0]) > 10:
				(m, r, t) = self.scrolling_message[0]
				a = []
				exp = HorizScroll(m).expand(padding, wraparound = r)
				if t == None:
					t = 0.1
				else:
					t = t / len(exp)
				for x in exp:
					a.append((x, r, t))
				del self.scrolling_message[0]
				self.scrolling_message = a + self.scrolling_message
			newmsg = self.scrolling_message[0]
			if newmsg[2] != None:
				self.next_update = time() + newmsg[2]
			else:
				self.next_update = None
			self.v.display(self.scrolling_message[0][0])
			if self.scrolling_message[0][1]:
				self.scrolling_message.append(self.scrolling_message[0])
			del self.scrolling_message[0]

	def done(self):
		return len(self.scrolling_message) == 0

