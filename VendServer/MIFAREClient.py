from .MIFAREDriver import MIFAREReader, MIFAREException
from serial import Serial

class MIFAREClient:
    def __init__(self):
        self.port = Serial('/dev/ttyS2', baudrate = 19200)
        self.reader = MIFAREReader(self.port)
        self.reader.set_led(red = False, green = True)
        self.reader.beep(100)
    
    def get_card_id(self):
        self.reader.set_led(red = True, green = False)
        try:
            card_id, capacity = self.reader.select_card()
        except MIFAREException:
            self.reader.set_led(red = False, green = True)
            return None
        else:
            self.reader.set_led(red = False, green = True)
            self.reader.beep(100)
            return card_id
