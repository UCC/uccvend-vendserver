.. :changelog:

History
-------

1.0 (2014-11-13)
---------------------

* First release on PyPI.


1.1 (2017-02-19)
---------------------

* Move MIFARE to dispense database
