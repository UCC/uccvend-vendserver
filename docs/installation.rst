============
Installation
============

At the command line::

    $ easy_install uccvend-vendserver

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv uccvend-vendserver
    $ pip install uccvend-vendserver
