
The authoritative repository for UCC Vendserver is:

https://gitlab.ucc.asn.au/UCC/uccvend-vendserver

This repository holds the python code that speaks to the UCC Snack Machine.

See also:
 https://gitlab.ucc.asn.au/UCC/uccvend-docs
