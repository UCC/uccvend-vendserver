#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


readme = open('README.md').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

requirements = [
    # TODO: put package requirements here
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='uccvend-vendserver',
    version='1.1',
    description='UCC Snack Machine Server Code',
    long_description=readme + '\n\n' + history,
    author='ACC Murphy',
    author_email='wheel@ucc.asn.au',
    url='http://git.ucc.asn.au/?p=uccvend-vendserver.git;a=summary',
    packages=[
        'VendServer',
    ],
    package_dir={'VendServer':
                 'VendServer'},
    data_files = [
        (
            os.path.join('/etc', 'init.d'),
            ['bin/init.d/vendserver',
	    ]
        )
    ],
    entry_points={
        "console_scripts": [
            "vendserver = VendServer.VendServer:main"
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="BSD",
    zip_safe=False,
    keywords='uccvend-vendserver',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
