"""
TracebackPrinter.py - Prints the current stack trace to a specified
file when SIGUSR1 is received.
---
Author: James Arcus <jimbo@ucc.asn.au>
Based-On: python-traceback-signal
          <https://github.com/angelcam/python-traceback-signal>
Author: Angelcam <dev@angelcam.com>
"""

import os
import sys
import traceback
import signal

def print_traceback(wfd, frame):
    msg = "Traceback signal received.\nTraceback (most recent call last):\n"
    msg += ''.join(traceback.format_stack(frame))
    os.write(wfd, msg)

def register_sigusr(wfd):
    def sigusr_handler(_, frame):
        # first param is which signal
        print_traceback(wfd, frame)

    signal.signal(signal.SIGUSR1, sigusr_handler)

def traceback_init(f):
    wfd = os.open(f, os.O_WRONLY | os.O_APPEND | os.O_CREAT, 0o600)
    register_sigusr(wfd)
